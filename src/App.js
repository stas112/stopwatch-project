import React, {useEffect} from "react";
import './App.css';

function App() {

  const[timeStart, setTime] =  React.useState(0)
  const[timerOn, setTimeOn] = React.useState(false)

useEffect(()=>{
  let interval 

  if(timerOn){
    interval = setInterval(()=>{
      setTime(previousTime => previousTime + 10)
    },10)
  }else{
    clearInterval(interval)
  }

  return()=> clearInterval(interval)

}, [timerOn])


 {
   /*double click on 'Wait' button*/
 }

const handleClick = (e) => {
  switch (e.detail) {
    case 2:
      setTimeOn(false)
        break;
    default:
        return;
  }
}

  return (
    <div className="App">
            <span>{("0" + Math.floor((timeStart / 60000) % 60))}:</span>
            <span>{("0" + Math.floor((timeStart / 1000) % 60))}:</span>
            <span>{("0" + Math.floor((timeStart / 10) % 100)).slice(-2)}</span>
      <div>
        {!timerOn && (
           <button onClick={()=>setTimeOn(true)}>Start</button>

        )}
         {timerOn && (
           <button onClick={()=>setTimeOn(false)}>Stop</button>

        )}
        <button onClick={handleClick}>Wait</button>
        <button onClick={()=>setTime(0)}>Reset</button>

      </div>
    </div>
  );
}

export default App;
